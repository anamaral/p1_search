# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    """

    #print("Start:", problem.getStartState())
    #print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    #print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    
    "*** YOUR CODE HERE ***"

    # A loop starts while the frontier stack is not empty.
    # A node (node) is removed and the path to that node (path_to_node) is removed from the frontier stack.
    # If the node is a target state based on the problem, the path to that node is returned.
    # For each successor of the current node, it is checked if it has already been expanded to avoid cycles. 
    # If it has not been expanded, it is added to the expanded list and inserted into the frontier stack along with the updated path.
    # If the frontier stack is empty and no path has been found to the target state, 
    # an empty list [] is returned to indicate that no solution was found.

    frontier = util.Stack()
    start = problem.getStartState()
    frontier.push((start, []))

    expanded = []

    while not frontier.isEmpty():
        node, path_to_node= frontier.pop()

        if problem.isGoalState(node):
            return path_to_node
        if node not in expanded:
            expanded.append(node)
            for each in problem.getSuccessors(node):
                if each[0] not in expanded:
                    frontier.push((each[0], path_to_node + [each[1]]))
    return []
    "util.raiseNotDefined()"

def breadthFirstSearch(problem: SearchProblem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"

    # Checks whether the initial state is a target state. If it is, an empty list [] is returned to indicate that no moving is needed.
    # A loop starts while the frontier queue is not empty.
    # A node (node) is removed and the path to that node (path_to_node) is removed from the frontier queue.
    # If the node is a target state based on the problem, the path to that node is returned.
    # For each successor of the current node, it is checked if it has already been expanded to avoid cycles. 
    # If it has not been expanded, it is added to the expanded list and inserted into the frontier queue along with the updated path.
    # If the frontier queue is empty and no path has been found to the target state, 
    # an empty list [] is returned to indicate that no solution was found.

    frontier = util.Queue()
    start = problem.getStartState()
    frontier.push((start, []))

    expanded = []

    if problem.isGoalState(start):
        return []
    
    while not frontier.isEmpty():
        node, path_to_node= frontier.pop()

        if problem.isGoalState(node):
            return path_to_node
        
        if node not in expanded:
            expanded.append(node)
            
            for each in problem.getSuccessors(node):
                if each[0] not in expanded:
                    frontier.push((each[0], path_to_node + [each[1]]))
    return []

    "util.raiseNotDefined()"

def uniformCostSearch(problem: SearchProblem):
    """Search the node of least total cost first."""

    # A loop starts while the frontier queue is not empty.
    # A node (node) is removed and the path to that node (path_to_node) is removed from the frontier queue. 
    # In addition, the cumulative cost to reach that node is obtained using problem.getCostOfActions(path_to_node).
    # Target Verification: If the node is a target state based on the problem, the path to that node is returned.
    # Node Expansion: For each successor of the current node, it is checked if it has already been expanded to avoid cycles. 
    # If it has not been expanded, it is added to the expanded list and inserted into the frontier queue along with the updated path and new accumulated cost.
    # If the frontier queue is empty and no path has been found to the target state, 
    # an empty list [] is returned to indicate that no solution was found.

    frontier = util.PriorityQueue()
    start = problem.getStartState()
    frontier.push((start, []),0)

    expanded = []

    while not frontier.isEmpty():
        node, path_to_node= frontier.pop()
        cost = problem.getCostOfActions(path_to_node)

        if problem.isGoalState(node):
            return path_to_node
        if node not in expanded:
            expanded.append(node)
            for each in problem.getSuccessors(node):
                if each[0] not in expanded:
                    frontier.push((each[0], path_to_node + [each[1]]), cost + each[2])
    return []
    "util.raiseNotDefined()"

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    # A loop starts while the frontier queue is not empty.
    # A node (node) is removed and the path to that node (path_to_node) is removed from the frontier queue.
    # If the node is a target state based on the problem, the path to that node is returned.
    # For each successor of the current node, it is checked if it has already been expanded to avoid cycles. 
    # If it has not been expanded, it is added to the expanded list and inserted into the frontier queue along with the updated path and the new total cost estimate.
    # If the frontier queue is empty and no path has been found to the target state, 
    # an empty list [] is returned to indicate that no solution was found.
    
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    n = Directions.NORTH
    e = Directions.EAST

    def a_heuristic(state, heuristic): 
        return heuristic(state[0], problem) + problem.getCostOfActions(state[1])

    frontier = util.PriorityQueueWithFunction(a_heuristic)
    expanded = []

    frontier.push((problem.getStartState(),[]),heuristic)

    while not frontier.isEmpty():

        node, path_to_node= frontier.pop()
    
        if problem.isGoalState(node):
            return path_to_node
        if node not in expanded:
            expanded.append(node)
            for each in problem.getSuccessors(node):
                if each[0] not in expanded:
                    frontier.push((each[0], path_to_node + [each[1]]),heuristic)
    return []    
    "util.raiseNotDefined()"


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
